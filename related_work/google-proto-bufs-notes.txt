google protocol buffers: used for serializing and retrieving structured data.  serialize means, put in a format to go across the wire (whatever the wire is).  

Write a .proto description of the data structure.  compiler "creates a class that implements automatic
encoding and parsing of the protocol buffer data with an efficient binary format"  the class provides
getters and setters for the fields of the protocol buffer. 

Why not use this?  1. does not support C. because it creates classes, which C does not have.  Future support for C? i don't know.

Compare this to projections.   Maybe that is the useful relation here. 

In protobufs a message describes a structure, just as a projection describes a
structure in our case.

And you can of course have a message be a field of another message.  

protobufs allows you to mark fields with unique tags.  we do not tag fields

protobufs has required fields and optional fields.  a value for a required field must be provided.  an optional field does not require a value to be provided.

have repeated fields (dynamically sized arrays).

protocol bufs generates a class for each message defined in the .proto file

each field in the message has a getter and setter func.  required and optional fields have a has func. also have a clear func.

the generated classes have methos for writing and reading messages of the chosen type using the proto buf binary format.

can serialize to string and (something else?) depending on the source language.

Can populate the fields to our values. then serialize to a string. then send somewhere
and unserialize. and get the values out.


The user includes the protocol buffer file in their code and writes their own code which uses it.
